<?php
namespace Tests\Leos\Component\Transaction\Model;

use Leos\Component\Transaction\Exception\NotEnoughFoundsException;
use Leos\Component\Transaction\Model\DebitMovement;
use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\User\Definition\UserInterface;
use Tests\Leos\Component\User\Model\UserTest;
use Leos\Component\Transaction\Model\Transaction;
use Leos\Component\Transaction\Model\CreditMovement;
use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Transaction\Definition\MovementInterface;

/**
 * Class TransactionTest
 *
 * @package Tests\Leos\Component\Transaction\Model
 */
class TransactionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @group unit
     */
    public function testCreditTransaction()
    {
        $transaction = $this->getTransaction(new CreditMovement(
            TransactionCategoryTest::getTransactionCategory(TransactionCategory::WIN),
            50.00, 25.00));

        $this->assertNotNull($transaction->getId());
        $this->assertNotNull($transaction->getUser());

        $this->assertEquals(5000, $transaction->getAmountReal());
        $this->assertEquals(2500, $transaction->getAmountBonus());

        $this->assertEquals(0, $transaction->getAmountRealOld());
        $this->assertEquals(0, $transaction->getAmountBonusOld());

        $this->assertTrue(($transaction->getCurrency() instanceof CurrencyInterface));
    }

    /**
     * @group unit
     */
    public function testDebitTransactionOK()
    {
        $user = UserTest::getUser();

        $this->getTransaction(new CreditMovement(
            TransactionCategoryTest::getTransactionCategory(TransactionCategory::WIN),
            50.00, 25.00), $user);

        $transaction = $this->getTransaction(new DebitMovement(
            TransactionCategoryTest::getTransactionCategory(TransactionCategory::BET),
            0, 25.00), $user);

        $this->assertNotNull($transaction->getId());
        $this->assertNotNull($transaction->getUser());

        $this->assertEquals(0, $transaction->getAmountReal());
        $this->assertEquals(2500, $transaction->getAmountBonus());

        $this->assertEquals(0, $transaction->getAmountRealOperation());
        $this->assertEquals(-2500, $transaction->getAmountBonusOperation());

        $this->assertEquals(5000, $transaction->getAmountRealOld());
        $this->assertEquals(2500, $transaction->getAmountBonusOld());

        $this->assertTrue(($transaction->getCurrency() instanceof CurrencyInterface));
    }

    /**
     * @group unit
     */
    public function testDebitTransactionKO()
    {
        try {
            $this->getTransaction(new DebitMovement(
                TransactionCategoryTest::getTransactionCategory(TransactionCategory::BET),
                50.00, 25.00));
        } catch (NotEnoughFoundsException $e) {

            $this->assertNotNull($e->getCredit());
            $this->assertNotNull($e->getKey());
        }

    }

    /**
     * @param MovementInterface $movementInterface
     * @param UserInterface|null $user
     * @return Transaction
     */
    public static function getTransaction(MovementInterface $movementInterface, UserInterface $user = null): Transaction
    {
        return new Transaction($user ?: UserTest::getUser(), $movementInterface);
    }
}
