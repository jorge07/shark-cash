<?php
namespace Tests\Leos\Component\Play\Manager;

use Leos\Component\Play\Manager\PlayManager;
use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;
use Leos\Component\Play\Manager\PlaySessionManager;
use Leos\Component\Play\Factory\PlayFactoryInterface;
use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\Play\Repository\PlayRepositoryInterface;
use Leos\Component\Play\Factory\PlaySessionFactoryInterface;
use Leos\Component\Play\Factory\PlaySessionRoundFactoryInterface;
use Leos\Component\Play\Repository\PlaySessionRepositoryInterface;
use Leos\Component\Play\Repository\PlaySessionRoundRepositoryInterface;

use Tests\Leos\Component\User\Model\UserTest;
use Tests\Leos\Component\Play\Model\PlayTest;
use Tests\Leos\Component\Play\Model\PlaySessionTest;

/**
 * Class PlaySessionManagerTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Manager
 */
class PlaySessionManagerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var PlaySession
     */
    private $session;

    public function setUp()
    {
        $this->session = $this->getSession();
    }

    /**
     * @group unit
     */
    public function testPlaySessionManager()
    {
        $manager = $this->getPlaySessionManager();

        $session = $manager->findSessionOrFail(UserTest::getUser(), "session", new \Exception("session not found test"));
        $round = $manager->findOneRoundBySessionAndRoundIdOrFail($this->session, "round", new \Exception("round not found test"));

        $this->assertTrue($session instanceof  PlaySession);
        $this->assertTrue($round instanceof  PlaySessionRound);

        $session = $manager->play(
            UserTest::getUser(),
            TransactionCategory::WIN,
            50.00,
            25.00,
            [
                'session' => $sessionId ="session",
                'round' => $roundId = "round",
                'close' => false,
            ]);

        $this->assertEquals($sessionId, $session->getSession());
        $this->assertEquals($roundId, $session->getRound($roundId)->getRoundId());
        $this->assertEquals(5000, $session->getBetsAmountReal());
        $this->assertEquals(2500, $session->getBetsAmountBonus());
        $this->assertEquals(1, $session->getBets());
        $this->assertEquals(1, $session->getWins());
        $this->assertEquals(1, $session->getGame());
        $this->assertEquals(5000, $session->getWinsAmountReal());
        $this->assertEquals(2500, $session->getWinsAmountBonus());
    }

    /**
     * @return PlaySessionManager
 */
    protected function getPlaySessionManager()
    {
        return new PlaySessionManager(
                $this->getSessionRepositoryMock(),
                $this->getRoundRepositoryMock(),
                $this->getSessionFactory(),
                $this->getSessionRoundFactory(),
                $this->getPlayManager()
            )
        ;
    }

    /**
     * @return PlaySessionFactoryInterface
     */
    private function getSessionFactory()
    {
        return $this->getMockBuilder(PlaySessionFactoryInterface::class)->getMock();
    }

    /**
     * @return PlaySessionRoundFactoryInterface
     */
    private function getSessionRoundFactory()
    {
        return $this->getMockBuilder(PlaySessionRoundFactoryInterface::class)->getMock();
    }

    /**
     * @return PlayManager
     */
    private function getPlayManager()
    {
        return new PlayManager(
            $this->getPlayFactory(),
            $this->getPlayRepository()
        );
    }

    /**
     * @return PlayRepositoryInterface
     */
    private function getPlayRepository()
    {
        return $this->getMockBuilder(PlayRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

    }

    /**
     * @return PlayFactoryInterface
     */
    private function getPlayFactory()
    {
        $mock = $this->getMockBuilder(PlayFactoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $mock->method('process')
            ->willReturn(PlayTest::getPlay(1, "session", "round", TransactionCategory::WIN))
        ;

        return $mock;

    }

    /**
     * @return PlaySessionRepositoryInterface
     */
    private function getSessionRepositoryMock()
    {
        $repo = $this
            ->getMockBuilder(PlaySessionRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $session = $this->getSession();

        $repo
            ->expects($this->atLeastOnce())
            ->method('findOneByUserAndSession')
            ->will($this->returnValue(
                $session
            ))
        ;

        return $repo;
    }

    /**
     * @return PlaySessionRoundRepositoryInterface
     */
    protected function getRoundRepositoryMock()
    {
        $repo = $this
            ->getMockBuilder(PlaySessionRoundRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $session = $this->getSession();

        $repo
            ->expects($this->atLeastOnce())
            ->method('findOneBySessionAndRound')
            ->will($this->returnValue($session->getRound("round")))
        ;

        return $repo;
    }

    /**
     * @return PlaySession
     */
    private function getSession()
    {
        return PlaySessionTest::getPlaySession("session")
            ->addTransaction(
                PlayTest::getPlay(1, "session", "round", TransactionCategory::BET)
            )
        ;
    }
}
