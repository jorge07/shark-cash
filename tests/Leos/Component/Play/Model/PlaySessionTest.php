<?php
namespace Tests\Leos\Component\Play\Model;

use Leos\Bundle\TransactionBundle\Entity\TransactionCategory;
use Leos\Component\Play\Model\PlaySession;
use Tests\Leos\Component\User\Model\UserTest;

/**
 * Class PlaySessionTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Model
 */
class PlaySessionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @group unit
     */
    public function testPlaySessionBetAndWin()
    {
        $session = self::getPlaySession();

        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::BET
            )
        );

        $this->assertNotNull($session->getSession());
        $this->assertNotNull($session->getCurrentPlay());
        $this->assertCount(1, $session->getPlayTransactions());
        $this->assertCount(1, $session->getRounds());

        $this->assertEquals(1, $session->getBets());
        $this->assertEquals(0, $session->getWins());

        $this->assertNotNull($session->getBetsAmountReal());
        $this->assertNotNull($session->getBetsAmountBonus());

        $this->assertNotNull($session->getGame());


        $currentPlay = $session->getCurrentPlay();

        $this->assertNotNull($currentPlay->getPlaySession());
        $this->assertNotNull($currentPlay->getPlaySessionRound());

        $this->assertEquals(5000, $currentPlay->getB2bReal());
        $this->assertEquals(2500, $currentPlay->getB2bBonus());


        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::WIN
            )
        );

        $this->assertCount(2, $session->getPlayTransactions());
        $this->assertCount(1, $session->getRounds());

        $this->assertEquals(1, $session->getBets());
        $this->assertEquals(1, $session->getWins());
    }

    /**
     * @group unit
     */
    public function testPlaySessionBet()
    {
        $session = self::getPlaySession();

        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::BET
            )
        );

        $this->assertNotNull($session->getSession());
        $this->assertNotNull($session->getCurrentPlay());
        $this->assertCount(1, $session->getPlayTransactions());
        $this->assertCount(1, $session->getRounds());

        $this->assertEquals(1, $session->getBets());
        $this->assertEquals(0, $session->getWins());

        $this->assertNotNull($session->getBetsAmountReal());
        $this->assertNotNull($session->getBetsAmountBonus());

        $this->assertNotNull($session->getGame());


        $currentPlay = $session->getCurrentPlay();

        $this->assertNotNull($currentPlay->getPlaySession());
        $this->assertNotNull($currentPlay->getPlaySessionRound());

        $this->assertEquals(5000, $currentPlay->getB2bReal());
        $this->assertEquals(2500, $currentPlay->getB2bBonus());


        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::ROLLBACK_BET
            )
        );

        $this->assertCount(2, $session->getPlayTransactions());

        $this->assertCount(1, $session->getRounds());

        $this->assertEquals(0, $session->getBets());
        $this->assertEquals(0, $session->getWins());
    }
    /**
     * @group unit
     */
    public function testPlaySessionRollbackWin()
    {
        $session = self::getPlaySession();

        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::WIN
            )
        );

        $this->assertNotNull($session->getSession());
        $this->assertNotNull($session->getCurrentPlay());

        $this->assertCount(1, $session->getPlayTransactions());
        $this->assertEquals(0, $session->getBets());
        $this->assertEquals(1, $session->getWins());

        $this->assertNotNull($session->getWinsAmountReal());
        $this->assertNotNull($session->getWinsAmountBonus());

        $this->assertNotNull($session->getGame());

        $this->assertNotNull($session->getCurrentPlay()->getPlaySession());
        $this->assertNotNull($session->getCurrentPlay()->getPlaySessionRound());

        $session->addTransaction(
            PlayTest::getPlay(
                PlayTest::TEST_GAME_OK_ID[0],
                PlayTest::TEST_SESSION_ID,
                PlayTest::TEST_ROUND_ID,
                TransactionCategory::ROLLBACK_WIN
            )
        );

        $this->assertNotNull($session->getSession());
        $this->assertNotNull($session->getCurrentPlay());

        $this->assertCount(2, $session->getPlayTransactions());
        $this->assertEquals(0, $session->getBets());
        $this->assertEquals(0, $session->getWins());

        $this->assertEquals(0, $session->getWinsAmountReal());
        $this->assertEquals(0, $session->getWinsAmountBonus());

        $this->assertNotNull($session->getGame());

        $this->assertNotNull($session->getCurrentPlay()->getPlaySession());
        $this->assertNotNull($session->getCurrentPlay()->getPlaySessionRound());
    }

    /**
     * @param string|null $session
     * 
     * @return PlaySession
     */
    public static function getPlaySession(string $session = ""): PlaySession
    {
        return new PlaySession(UserTest::getUser(), $session);
    }
}

