<?php
namespace Tests\Leos\Component\Wallet\Model;
use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Wallet\Model\Currency;

/**
 * Class CurrencyTest
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Model
 */
class CurrencyTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @group unit
     */
    public function testCurrency()
    {
        $currency = self::getCurrency('eur', 1);

        $this->assertEquals('eur', $currency->getCode());
        $this->assertEquals(1, $currency->getExchangeRate());
    }

    /**
     * @param string $code
     * @param float $exchange
     * @return CurrencyInterface
     */
    public static function getCurrency(string $code, float $exchange): CurrencyInterface
    {
        return new Currency($code, $exchange);
    }
}