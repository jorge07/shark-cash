<?php
namespace Tests\Leos\Component\User\Fixture;

use Leos\Component\User\Model\User;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Wallet\Definition\WalletInterface;
use Leos\Component\Wallet\Model\Wallet;

/**
 * Class UserWalletAwareTest
 *
 * @package Leos\Component\User\Fixture
 */
class UserWalletAwareTest extends User implements WalletAwareInterface
{
    /**
     * @var WalletInterface
     */
    private $wallet;

    /**
     * UserWalletAwareTest constructor.
     *
     * @param string $username
     * @param array $roles
     */
    public function __construct(string $username, array $roles = [])
    {
        parent::__construct($username, $roles);

        $this->wallet = new Wallet();

    }

    /**
     * @return WalletInterface
     */
    public function getWallet(): WalletInterface
    {
        return $this->wallet;
    }
}
