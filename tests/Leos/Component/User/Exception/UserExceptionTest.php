<?php
namespace Tests\Leos\Component\User\Exception;

use Leos\Component\User\Exception\UserAlreadyExistException;

/**
 * Class UserExceptions
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Exception
 */
class UserExceptionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @group unit
     */
    public function testUserAlreadyExist()
    {
        try {

            throw new UserAlreadyExistException();

        } catch (UserAlreadyExistException $e) {

            $this->assertContains("username_already_exist", $e->getMessage());
        }
    }
}