Feature: User Api
  As a user
  I want to call the user API

  Background:
    Given An empty database with fixtures loaded


  Scenario: I wanna list the users
    When I send a GET request to "/api/v1/users.json"
    Then the response code should be 200
    And the response should contain "Paco"
    And the response should contain "_links"
    And the response should contain "self"

    When I send a GET request to "/api/v1/users.json?page=2&limit=3"
    Then the response code should be 200
    And the response should contain "_links"
    And the response should contain "self"

    When I send a GET request to "/api/v1/users.json?page=100&limit=100"
    Then the response code should be 404

  Scenario: I wanna see the user asd that not exist
    When I send a GET request to "/api/v1/user-error.json"
    Then the response code should be 404
    And the response should contain "error"

    When I send a GET request to "/api/v1/users/asd.json"
    Then the response code should be 404

  Scenario: I wanna see the user paco
    When I send a GET request to "/api/v1/users/paco.json"
    Then the response code should be 200
    And the response should contain "Paco"

  Scenario Outline: I wanna create a new user a list him
    When I send a POST request to "/api/v1/users.json" with body:
    """
    {
      "username": "<username>",
      "plain_password": {
        "password": "<plain_password>",
        "confirm": "<plain_password>"
      },
      "state": "<state>",
      "currency": <currency>
    }
    """
    Then the response code should be 201
    And I should be redirected to the resource
    And the response code should be 200
    And the response should contain "<username>"
    And the response should contain "wallet"
    And the response should contain "username"
    And the response should not contain "password"

    Then I send a PATCH to the resource with body:
    """
    {
      "plain_password": {
        "password": "<newPassword>",
        "confirm": "<newPassword>"
     }
    }
    """
    And the response code should be 202

    Then I send a PATCH to the resource with values:
      | state | <status> |
    And the response code should be 202

    Then I send a PATCH to the resource with values:
      | currency |  -2 |
    And the response code should be 400

    Then I send a DELETE to the resource
    And the response code should be 204

    Examples:
      | username   | plain_password | state | newPassword | status | currency |
      | jorge      | qwerty         | 1     | qwertyuiop  | 2      |   EUR    |
      | mario      | qwerty2        | 1     | poiuytrewq  | 2      |   GBP    |

  Scenario Outline: I wanna try to create a new user with empty name
    When I send a POST request to "/api/v1/users.json" with body:
    """
    {
      "username": "<username>",
      "plain_password": {
        "password": "<plain_password>",
        "confirm": "<plain_password>"
     },
      "state": "<state>",
      "currency": <currency>
    }
    """
    Then the response code should be 400

    Examples:
      | username   | plain_password | state | currency |
      | jorge      |                | 1     |   EUR    |
      | mario      | qwerty2        |       |   EUR    |
      | mario      | qwerty2        | 1     |   2      |
      |            | qwerty2        | 1     |   EUR    |

  Scenario Outline: I wanna try to create a new user without name
    When I send a POST request to "/api/v1/users.json" with body:
    """
    {
      "plain_password": {
        "password": "<plain_password>",
        "confirm": "<plain_password>"
     },
      "state": "<state>",
      "currency": <currency>
    }
    """
    Then the response code should be 400

    Examples:
      | plain_password | state | currency |
      | qwerty2        | 1     |   EUR    |

  Scenario Outline: User exist
    When I send a POST request to "/api/v1/users.json" with body:
    """
    {
      "username": "<username>",
      "plain_password": {
        "password": "<plain_password>",
        "confirm": "<plain_password>"
     },
      "state": "<state>",
      "currency": <currency>
    }
    """
    Then the response code should be 409

    Examples:
      | username   | plain_password | state | currency |
      | barry      | qweqwr         | 1     |   EUR    |
