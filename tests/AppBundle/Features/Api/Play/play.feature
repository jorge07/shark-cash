Feature: Play Api
  I wanna test the gamimg transactions api

  Background:
    Given An empty database with fixtures loaded

  Scenario: I try to get a non existing transaction
    When I send a GET request to "/api/v1/plays/1.json"
    Then the response code should be 404

  Scenario Outline: I wanna create a new play bet transaction with no founds
    When I send a POST request to "/api/v1/plays/bets.json" with values:
      | session        | <session> |
      | round          | <round>   |
      | user           | <user>    |
      | real           | <real>    |
      | bonus          | <bonus>   |
    Then the response code should be 409

    Examples:
      | session | round | user  | real    | bonus  |
      | 1234567 | 76543 | paco  | 150.00  | 125.00 |
      | 1234567 | 76543 | malin | 150.00  | 100.20 |
      | 1234567 | 76543 | barry |         | 125.00 |

  Scenario Outline: I wanna try create a new play win transaction but fails
    When I send a POST request to "/api/v1/plays/wins.json" with values:
      | session        | <session> |
      | round          | <round>   |
      | user           | <user>    |
      | real           | <real>    |
      | bonus          | <bonus>   |
    Then the response code should be 400

    Examples:
      | session | round | user  | real  | bonus |
      | 1234567 | 76543 |       | 50.00 | 25.00 |
      | 1234567 | 76543 | me    | 50.00 | 25.00 |
      | 1234567 |       | barry |       | 25.00 |
      |         | 12334 | barry |       | 25.00 |

  Scenario Outline: I wanna create a new play win transaction
    When I send a POST request to "/api/v1/plays/wins.json" with values:
      | session        | <session> |
      | round          | <round>   |
      | user           | <user>    |
      | real           | <real>    |
      | bonus          | <bonus>   |
      | close          | false     |

    Then the response code should be 201
    And I should be redirected to the resource
    And the response code should be 200
    And the response contains:
    """
      {
        "game":1,
        "session":"1234567",
        "play_session": {
          "game":1,
          "provider":1,
          "session":"1234567",
          "bets":0,
          "wins":1,
          "bets_amount_real":0,
          "bets_amount_bonus":0,
          "wins_amount_real":<realCredit>,
          "wins_amount_bonus":<bonusCredit>,
          "_links": {
            "user": {
              "href":"http://localhost/api/v1/users/<user>"
            },
            "plays": {
            }
          }
        },
        "round":"76543",
        "play_session_round": {
          "round_id":"76543",
          "closed":false,
          "bet_real":0,
          "bet_bonus":0,
          "win_real":<realCredit>,
          "win_bonus":<bonusCredit>
        },
        "transaction": {
          "category": {
            "name":"win"
          },
          "amount_real":<realCredit>,
          "amount_bonus":<bonusCredit>,
          "amount_real_old":0,
          "amount_bonus_old":0,
          "amount_real_operation":<realCredit>,
          "amount_bonus_operation":<bonusCredit>,
          "currency": {
            "code":"eur",
            "exchange":1
          },
          "_links":{
            "user": {
              "href":"http://localhost/api/v1/users/<user>"
            }
          }
        }
      }
    """
    And the response should contain "transaction"
    And the response should contain "id"
    And the response should contain "<realCredit>"
    And the response should contain "<bonusCredit>"

    Then I send a POST request to "/api/v1/plays/bets.json" with values:
      | session        | <session>  |
      | round          | <round>    |
      | user           | <user>     |
      | real           | <betReal>  |
      | bonus          | <betBonus> |
      | close          | false      |

    Then the response code should be 201
    And I should be redirected to the resource
    And the response code should be 200
    And the response contains:
    """
      {
        "game":1,
        "session":"1234567",
        "play_session": {
          "game":1,
          "provider":1,
          "session":"1234567",
          "bets":0,
          "wins":1,
          "bets_amount_real":<betReal>,
          "bets_amount_bonus":<betBonus>,
          "wins_amount_real":<realCredit>,
          "wins_amount_bonus":<bonusCredit>,
          "_links": {
            "user": {
              "href":"http://localhost/api/v1/users/<user>"
            },
            "plays": {
            }
          }
        },
        "round":"76543",
        "play_session_round": {
          "round_id":"76543",
          "closed":false,
          "bet_real":<betReal>,
          "bet_bonus":<betBonus>,
          "win_real":<realCredit>,
          "win_bonus":<bonusCredit>
        },
        "transaction": {
          "category": {
            "name":"win"
          },
          "amount_real":<realCredit>,
          "amount_bonus":<bonusCredit>,
          "amount_real_old":0,
          "amount_bonus_old":0,
          "amount_real_operation":<realCredit>,
          "amount_bonus_operation":<bonusCredit>,
          "currency": {
            "code":"eur",
            "exchange":1
          },
          "_links":{
            "user": {
              "href":"http://localhost/api/v1/users/<user>"
            }
          }
        }
      }
    """

    When I send a POST request to "/api/v1/plays/wins.json" with values:
      | session        | <session> |
      | round          | <round>   |
      | user           | <user>    |
      | real           | <real>    |
      | bonus          | <bonus>   |
      | close          | true      |

    Then the response code should be 201
    And I should be redirected to the resource
    And the response code should be 200
    And the response should contain "transaction"
    And the response should contain "id"
    And the response should contain "<realCredit>"
    And the response should contain "<bonusCredit>"

    When I send a GET request to "/api/v1/playsessions.json"
    And the response code should be 200
    And the response should contain "page"
    And the response should contain "total"
    And the response should contain "id"
    And the response should contain "<finalReal>"
    And the response should contain "<finalBonus>"

    Examples:
      | session | round | user  | real  | bonus | realCredit | bonusCredit | betReal | betBonus|  amountReal_old | finalReal | finalBonus|
      | 1234567 | 76543 | paco  | 50.00 | 25.00 |    5000    |    2500     |   25.00 |    0    |      10000      |   10000   |    2500   |
      | 1234567 | 76543 | malin | 50.00 | 00.20 |    5000    |     20      |   25.00 |    0    |      10000      |   10000   |     40    |
      | 1234567 | 76543 | barry |       | 25.00 |     0      |    2500     |     0   |  25.0   |      10000      |     0     |    5000   |
