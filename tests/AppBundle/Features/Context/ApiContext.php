<?php
namespace Tests\AppBundle\Features\Context;

use Behat\Behat\Context\Context,
    Behat\Behat\Context\SnippetAcceptingContext,
    Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode,
    Behat\Behat\Hook\Scope\BeforeScenarioScope;

use Leos\Bundle\I18nBundle\DataFixtures\ORM\LoadCurrencyData;
use Leos\Bundle\PlayBundle\DataFixtures\ORM\LoadPlayData;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\Finder,
    Symfony\Bundle\FrameworkBundle\Client,
    Symfony\Component\Filesystem\Filesystem,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

use Leos\Bundle\UserBundle\DataFixtures\ORM\LoadUserData,
    Leos\Bundle\TransactionBundle\DataFixtures\ORM\LoadTransactionCategoryData;

use Doctrine\Common\DataFixtures\Executor\AbstractExecutor;
use Liip\FunctionalTestBundle\Test\WebTestCase;


/**
 * Class ApiContext
 *
 * Class still under construction but at least it works right now
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 */
class ApiContext extends WebTestCase implements Context, SnippetAcceptingContext
{
    /**
     * @var string
     */
    protected $authorization;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $headers = array();

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $placeHolders = array();

    /**
     * @var AbstractExecutor
     */
    protected $fixtures;

    /**
     * @var \PHP_CodeCoverage|null
     */
    protected $coverage;

    /**
     * @var string|null
     */
    protected $scenario;

    /**
     * @var string
     */
    protected $sourceDir;

    /**
     * @var string
     */
    public $publicDir;

    protected $redirection = '';

    /**
     * UserApiContext constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setClient(
            self::createClient()
        );

        $this->sourceDir = $this->getContainer()->getParameter('kernel.root_dir') . "/../src";
        $this->publicDir = $this->getContainer()->getParameter('kernel.root_dir') . "/../web";
    }

    /**
     * @BeforeScenario
     *
     * @param BeforeScenarioScope $scenario
     */
    public function prepareScenario($scenario)
    {
//        echo "Preparing coverage feature for: " . $this->scenario . " \n";
//
//        $this->scenario = $name = str_replace( " ", "-",
//            ("Feature:". $scenario->getFeature()->getTitle() .
//                "->Scenario: ". ($scenario->getScenario()->getTitle() ?: uniqid(time()))) );
//
//        $filter = new \PHP_CodeCoverage_Filter();
//        $filter->addDirectoryToWhitelist($this->sourceDir);
//
//        $this->coverage = new \PHP_CodeCoverage(null, $filter);
//        $this->coverage->start('Behat::' . $name);

        $this->setClient(
            self::createClient()
        );
    }

    /**
     * @AfterScenario
     */
    public function cleanScenario()
    {
//        $this->coverage->stop();
//
//        $fs = new Filesystem();
//
//        if (!$fs->exists($this->publicDir ."/coverage")  ) {
//
//            $fs->mkdir($this->publicDir ."/coverage");
//        }
//
//        $writer = new \PHP_CodeCoverage_Report_PHP();
//        $writer->process($this->coverage, $file =  $this->publicDir . '/coverage/behat-cover-' . $this->scenario . '.php');
//        echo "Saved Scenario coverage feature in: " . $file . " \n";

        $this->client = null;
        $this->request = null;
        $this->response = null;
        $this->coverage = null;

    }

    /**
     * {@inheritdoc}
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Adds Basic Authentication header to next request.
     *
     * @param string $username
     * @param string $password
     *
     * @Given /^I am authenticating as "([^"]*)" with "([^"]*)" password$/
     */
    public function iAmAuthenticatingAs($username, $password)
    {
        $this->removeHeader('Authorization');
        $this->authorization = base64_encode($username . ':' . $password);
        $this->addHeader('Authorization', 'Bearer ' . $this->authorization);
    }

    /**
     * Sets a HTTP Header.
     *
     * @param string $name  header name
     * @param string $value header value
     *
     * @Given /^I set header "([^"]*)" with value "([^"]*)"$/
     */
    public function iSetHeaderWithValue($name, $value)
    {
        $this->addHeader($name, $value);
    }

    /**
     * Sends HTTP request to specific relative URL.
     *
     * @param string $method request method
     * @param string $url    relative url
     *
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)"$/
     */
    public function iSendARequest($method, $url)
    {
        $this->setClient(
            self::createClient()
        );
        $url = $this->prepareUrl($url);

        $this->client->request($method, $url, $this->headers);
        $this->request = $this->client->getRequest();

        $this->sendRequest();
    }

    /**
     * Sends HTTP request to specific URL with field values from Table.
     *
     * @param string    $method request method
     * @param string    $url    relative url
     * @param TableNode $post   table of post values
     *
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with values:$/
     */
    public function iSendARequestWithValues($method, $url, TableNode $post)
    {
        $this->setClient(
            self::createClient()
        );

        $url = $this->prepareUrl($url);
        $fields = array();

        foreach ($post->getRowsHash() as $key => $val) {
            $fields[$key] = $this->replacePlaceHolder($val);
        }

        $url = $this->prepareUrl($url);

        $this->client->request($method, $url, [], [], ['CONTENT_TYPE'=> 'application/json'], json_encode($fields));
        $this->request = $this->client->getRequest();

        $this->sendRequest();
    }

    /**
     * Sends HTTP request to specific URL with raw body from PyString.
     *
     * @param string       $method request method
     * @param string       $url    relative url
     * @param PyStringNode $string request body
     *
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with body:$/
     */
    public function iSendARequestWithBody($method, $url, PyStringNode $string)
    {
        $this->setClient(
            self::createClient()
        );

        $url = $this->prepareUrl($url);
        $string = $this->replacePlaceHolder(trim($string));

        $params = json_decode($string, true);

        $this->client->request($method, $url, $params, [], ['CONTENT_TYPE' => 'application/json']);
        $this->request = $this->client->getRequest();

        $this->sendRequest();
    }

    /**
     * Sends HTTP request to specific URL with form data from PyString.
     *
     * @param string       $method request method
     * @param string       $url    relative url
     * @param PyStringNode $body   request body
     *
     * @When /^(?:I )?send a ([A-Z]+) request to "([^"]+)" with form data:$/
     */
    public function iSendARequestWithFormData($method, $url, PyStringNode $body)
    {
        $this->setClient(
            self::createClient()
        );

        $url = $this->prepareUrl($url);
        $body = $this->replacePlaceHolder(trim($body));

        $fields = array();
        parse_str(implode('&', explode("\n", $body)), $fields);

        $this->client->request($method, $url, ['Content-Type' => 'application/x-www-form-urlencoded'], [], http_build_query($fields, null, '&'));
        $this->request = $this->client->getRequest();

        $this->sendRequest();
    }

    /**
     * Checks that response has specific status code.
     *
     * @param string $code status code
     *
     * @Then /^(?:the )?response code should be (\d+)$/
     */
    public function theResponseCodeShouldBe($code)
    {
        $expected = intval($code);
        $actual = intval($this->response->getStatusCode());
        $this->assertSame($expected, $actual);
    }

    /**
     * Checks that response body contains specific text.
     *
     * @param string $text
     *
     * @Then /^(?:the )?response should contain "([^"]*)"$/
     */
    public function theResponseShouldContain($text)
    {
        $expectedRegexp = '/' . preg_quote($text) . '/i';
        $actual = (string) $this->response->getContent();
        $this->assertRegExp($expectedRegexp, $actual);
    }

    /**
     * Checks that response body doesn't contains specific text.
     *
     * @param string $text
     *
     * @Then /^(?:the )?response should not contain "([^"]*)"$/
     */
    public function theResponseShouldNotContain($text)
    {
        $expectedRegexp = '/' . preg_quote($text) . '/';
        $actual = (string) $this->response->getContent();
        $this->assertNotRegExp($expectedRegexp, $actual);
    }

    /**
     * Checks that response body is exactly
     *
     * @param PyStringNode $stringNode
     *
     * @Then /^(?:the )?response contains:$/
     */
    public function theResponseContains(PyStringNode $stringNode)
    {
        $data = json_decode($stringNode->getRaw(), true);
        $jsonResponse = json_decode($this->response->getContent(), true);

        $this->compare($data, $jsonResponse);
    }

    /**
     * @param array $data
     * @param array $expect
     *
     * @return \Exception
     */
    private function compare(array $data, array $expect)
    {
        foreach ($data as $property => $value) {

            if (!isset($expect[$property])) {

                $this->assertEquals($property, $value);
                break;

            } elseif (is_array($value)) {

                $this->compare($value, $expect[$property]);

                break;

            }

            try{

                $this->assertEquals($value, $expect[$property]);

            } catch (\Exception $e) {

                $this->assertEquals($property, $value);
            }
        }
    }

    /**
     * Checks that response body contains JSON from PyString.
     *
     * Do not check that the response body /only/ contains the JSON from PyString,
     *
     * @param PyStringNode $jsonString
     *
     * @throws \RuntimeException
     *
     * @Then /^(?:the )?response should contain json:$/
     */
    public function theResponseShouldContainJson(PyStringNode $jsonString)
    {
        $etalon = json_decode($this->replacePlaceHolder($jsonString->getRaw()), true);
        $actual = json_decode($this->response->getContent(), true);

        if (null === $etalon) {
            throw new \RuntimeException(
                "Can not convert etalon to json:\n" . $this->replacePlaceHolder($jsonString->getRaw())
            );
        }

        if (null === $actual) {
            throw new \RuntimeException(
                "Can not convert actual to json:\n" . $this->replacePlaceHolder((string) $this->response->getContent())
            );
        }

        $this->assertGreaterThanOrEqual(count($etalon), count($actual));
        foreach ($etalon as $key => $needle) {
            $this->assertArrayHasKey($key, $actual);
            $this->assertEquals($etalon[$key], $actual[$key]);
        }
    }

    /**
     * Prints last response body.
     *
     * @Then print response
     */
    public function printResponse()
    {
        $request = $this->request;
        $response = $this->response;

        echo sprintf(
            "%s %s => %d:\n%s",
            $request->getMethod(),
            (string) $request->getUri(),
            $response->getStatusCode(),
            (string) $response->getContent()
        );
    }

    /**
     * Prepare URL by replacing placeholders and trimming slashes.
     *
     * @param string $url
     *
     * @return string
     */
    protected function prepareUrl($url)
    {
        return ltrim($this->replacePlaceHolder($url), '/');
    }

    /**
     * Sets place holder for replacement.
     *
     * you can specify placeholders, which will
     * be replaced in URL, request or response body.
     *
     * @param string $key   token name
     * @param string $value replace value
     */
    public function setPlaceHolder($key, $value)
    {
        $this->placeHolders[$key] = $value;
    }

    /**
     * Replaces placeholders in provided text.
     *
     * @param string $string
     *
     * @return string
     */
    protected function replacePlaceHolder($string)
    {
        foreach ($this->placeHolders as $key => $val) {
            $string = str_replace($key, $val, $string);
        }

        return $string;
    }

    /**
     * @Given /^An empty database with fixtures loaded$/
     */
    public function anEmptyDatabaseWithFixturesLoaded()
    {
        echo "Installing data fixtures\n";

        $this->fixtures = $this->loadFixtures([
            LoadCurrencyData::class,
            LoadTransactionCategoryData::class,
            LoadUserData::class,
            LoadPlayData::class
        ]);

        $this->setPlaceHolder(
            'EUR',
            $this->fixtures->getReferenceRepository()->getReference('currency-eur')->getId()
        );
        $this->setPlaceHolder(
            'GBP',
            $this->fixtures->getReferenceRepository()->getReference('currency-gbp')->getId()
        );
    }

    /**
     * @Given /^I should be redirected to the resource$/
     */
    public function iShouldBeRedirectedToTheResource()
    {
        $this->redirection = $this->response->headers->get('location');
        $this->headers = $this->response->headers->all();

        $this->iSendARequest('GET', $this->redirection);
    }

    /**
     * @Then /^I send a ([A-Z]+) to the resource$/
     */
    public function iSendAToTheResource($method)
    {
        $this->iSendARequest($method, $this->redirection);
    }

    /**
     * @When /^(?:I )?send a ([A-Z]+) to the resource with values:$/
     */
    public function iSendAToTheResourceWith($method, TableNode $node)
    {
        $this->iSendARequestWithValues($method, $this->redirection, $node);
    }


    /**
     * @When /^(?:I )?send a ([A-Z]+) to the resource with body:$/
     */
    public function iSendAToTheResourceWithBody($method, PyStringNode $node)
    {
        $this->iSendARequestWithBody($method, $this->redirection, $node);
    }

    /**
     * Returns headers, that will be used to send requests.
     *
     * @return array
     */
    protected function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Adds header
     *
     * @param string $name
     * @param string $value
     */
    protected function addHeader($name, $value)
    {
        if (isset($this->headers[$name])) {
            if (!is_array($this->headers[$name])) {
                $this->headers[$name] = array($this->headers[$name]);
            }

            $this->headers[$name][] = $value;
        } else {
            $this->headers[$name] = $value;
        }
    }

    /**
     * Removes a header identified by $headerName
     *
     * @param string $headerName
     */
    protected function removeHeader($headerName)
    {
        if (array_key_exists($headerName, $this->headers)) {
            unset($this->headers[$headerName]);
        }
    }

    /**
     * @throws \Exception
     */
    protected function sendRequest()
    {
        try {

            $this->response = $this->client->getResponse();

        } catch (\Exception $e) {

            $this->response = $e->getMessage();

            if (null === $this->response) {
                throw $e;
            }
        }
    }

    /**
     * Attempts to guess the kernel location.
     *
     * When the Kernel is located, the file is required.
     *
     * @return string The Kernel class name
     *
     * @throws \RuntimeException
     */
    protected static function getKernelClass()
    {

        $basePath = __DIR__ . "/../../../../";

        if (isset($_SERVER['KERNEL_DIR'])) {

            $dir = $_SERVER['KERNEL_DIR'];

            if (!is_dir($dir)) {

                $phpUnitDir = $basePath;

                if (is_dir("$phpUnitDir/$dir")) {

                    $dir = "$phpUnitDir/$dir";
                }
            }

        } else {

            $dir = $basePath . "app";
        }

        $finder = new Finder();
        $finder->name('*Kernel.php')->depth(0)->in($dir);

        $results = iterator_to_array($finder);

        if (!count($results)) {

            throw new \RuntimeException('Ops something was wrong overwriting WebTestCase::createKernel() method.');
        }

        $file = current($results);
        $class = $file->getBasename('.php');
        require_once $file;
        return $class;
    }

}
