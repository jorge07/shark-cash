<?php

namespace AppBundle\Controller\Api\Play;

use AppBundle\Controller\Controller;

use Leos\Bundle\PlayBundle\Entity\PlaySession;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PlaySessionController
 *
 * @package AppBundle\Controller\Api\Play
 * @RouteResource("PlaySession")
 */
class PlaySessionController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="returns all game sessions",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "List"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        return $this->get('leos.utils.pagination')->paginate(
            $this->get('leos.manager.session')->findAll(),
            'get_playsessions',
            [
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $paramFetcher->get('limit'),
            $paramFetcher->get('page')
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Given an id returns the play session",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Leos\Component\Play\Model\PlaySession"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Basic", "Embed"})
     *
     * @param int $id
     *
     * @return PlaySession
     *
     * @throws \Exception
     */
    public function getAction(int $id): PlaySession
    {
        return $this->get('leos.manager.session')->findOneSessionByIdOrFail($id,
            new NotFoundHttpException("Play transaction not found")
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="returns all game transactions for a sessions",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "List"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param int $id
     * @param ParamFetcher $paramFetcher
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetPlayAction(int $id, ParamFetcher $paramFetcher)
    {
        return $this->get('leos.utils.pagination')->paginate(
            $this->get('leos.manager.play')->findBySession($this->getAction($id)),
            'get_playsessions_play',
            [
                'id' => $id,
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $paramFetcher->get('limit'),
            $paramFetcher->get('page')
        );
    }
}
