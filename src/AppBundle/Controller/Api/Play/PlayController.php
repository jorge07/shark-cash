<?php

namespace AppBundle\Controller\Api\Play;

use AppBundle\Controller\Controller;

use Leos\Bundle\PlayBundle\Entity\Play;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;
use Leos\Bundle\TransactionBundle\Entity\TransactionCategory;

use Leos\Component\Play\Exception\NoSessionInRoundException;
use Leos\Component\Play\Exception\RoundAlreadyClosedException;
use Leos\Component\Transaction\Exception\NotEnoughFoundsException;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PlayController
 *
 * @package AppBundle\Controller\Api
 * @RouteResource("Play")
 */
class PlayController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="returns all game transactions",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "List"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        return $this->paginate(
            $this->get('leos.manager.play')->all(),
            'get_plays',
            [
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $paramFetcher->get('limit'),
            $paramFetcher->get('page')
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Given and id returns the reansaction",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Basic", "Embed"})
     *
     * @param int $id
     * @return \Leos\Component\Transaction\Model\Transaction
     * @throws \Exception
     */
    public function getAction(int $id)
    {
        return $this->get('leos.manager.play')->findOneByIdOrFail(
            
            $id, new NotFoundHttpException("Play transaction not found")
        );
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Generate a negative insertion on user wallet"
     * )
     *
     * @Rest\RequestParam(name="session", strict=false, nullable=false, description="The game session identifier")
     * @Rest\RequestParam(name="round", strict=false, nullable=false, description="The session round identifier")
     * @Rest\RequestParam(name="user", strict=false, nullable=false, description="The user involved in")
     * @Rest\RequestParam(name="real", strict=false, description="The real amount to credit")
     * @Rest\RequestParam(name="bonus", strict=false, description="The bonus amount to credit")
     * @Rest\RequestParam(name="close", strict=false, default="false", description="Close round")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @Rest\View(serializerGroups={"Basic"})
     *
     * @return Play
     */
    public function postBetAction(ParamFetcher $paramFetcher)
    {
        try {
            return $this->process($paramFetcher, TransactionCategory::BET);

        } catch(NotEnoughFoundsException $e) {

            throw new ConflictHttpException($e->getMessage());
        }
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Generate a positive insertion on user wallet"
     * )
     *
     * @Rest\RequestParam(name="session", strict=true, nullable=false, description="The game session identifier")
     * @Rest\RequestParam(name="round", strict=true, nullable=false, description="The session round identifier")
     * @Rest\RequestParam(name="user", strict=true, nullable=false, description="The user involved in")
     * @Rest\RequestParam(name="real", strict=true, description="The real amount to credit")
     * @Rest\RequestParam(name="bonus", strict=true, description="The bonus amount to credit")
     * @Rest\RequestParam(name="close", strict=false, default="false", description="Close round")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @Rest\View(serializerGroups={"Basic"})
     *
     * @return Play
     */
    public function postWinAction(ParamFetcher $paramFetcher)
    {
        return $this->process($paramFetcher, TransactionCategory::WIN);
    }

    /**
     * @param ParamFetcher $paramFetcher
     * @param string $category
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\FormInterface
     * @throws \Exception
     */
    protected function process(ParamFetcher $paramFetcher, string $category)
    {
        try {
            $session = $this
                ->get('leos.manager.session')
                ->play(
                    $this->get('leos.manager.user')
                        ->findOneBySlugOrFail(
                            $paramFetcher->get('user'),
                            new HttpException(400, "user.invalid_user")
                        ),
                    $category,
                    floatval($paramFetcher->get('real')),
                    floatval($paramFetcher->get('bonus')),
                    [
                        'session' => (string) $paramFetcher->get('session'),
                        'round' => (string) $paramFetcher->get('round'),
                        'close' => ('true' === $paramFetcher->get('close')),
                    ]
                )
            ;

        } catch(FormException $e) {

            return $e->getForm();

        } catch(NoSessionInRoundException $e) {

            throw new ConflictHttpException($e->getMessage());

        } catch(RoundAlreadyClosedException $e) {

            throw new ConflictHttpException($e->getMessage());
        }

        $id = $session->getCurrentPlay()->getId();

        return $this->routeRedirectView(
            'get_play',
            [
                'id' => $id,
            ],
            Response::HTTP_CREATED
        );
    }
}
