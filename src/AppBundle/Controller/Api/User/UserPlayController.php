<?php

namespace AppBundle\Controller\Api\User;

use AppBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * Class UserPlayController
 *
 * @package AppBundle\Controller\Api
 * @RouteResource("Play")
 */
class UserPlayController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Giver a user id returns all game transactions",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "List"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param string $slug
     * @param ParamFetcher $paramFetcher
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetAction(string $slug, ParamFetcher $paramFetcher)
    {
        return $this->get('leos.utils.pagination')->paginate(
            $this->get('leos.manager.play')->findByUser(
                $this->get('leos.controller.user')->getAction($slug)
            ),
            'get_plays',
            [
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $paramFetcher->get('limit'),
            $paramFetcher->get('page')
        );
    }
}
