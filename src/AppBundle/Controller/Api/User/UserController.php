<?php

namespace AppBundle\Controller\Api\User;

use AppBundle\Controller\Controller;

use Leos\Bundle\UserBundle\Entity\User;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;
use Leos\Component\User\Exception\UserAlreadyExistException;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController
 *
 * @package AppBundle\Controller\Api
 *
 * @Rest\Version("v1")
 *
 * @RouteResource("User")
 */
class UserController extends Controller
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     description="returns all users",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when page not found"
     *     },
     *     responseMap={
     *      200 = "Hateoas\Representation\PaginatedRepresentation"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Default", "Basic", "Details", "List"})
     *
     * @Rest\QueryParam(name="page", default="1", description="The page to display")
     * @Rest\QueryParam(name="limit", default="10", description="The number of items per pager")
     *
     * @param Request $request
     *
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function cgetAction(Request $request)
    {
        return $this->get('leos.utils.pagination')->paginate(
            $this->get('leos.manager.user')->all(),
            'get_users',
            [
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $request->get('limit', 10),
            $request->get('page', 1)
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="return a user for the given slug",
     *     statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions"
     *     },
     *     responseMap={
     *      200 = "Leos\Bundle\UserBundle\Entity\User"
     *     }
     * )
     *
     * @Rest\View(serializerGroups={"Basic", "Details"}, statusCode=200)
     *
     * @param string $slug
     * @return User
     */
    public function getAction(string $slug): User
    {
        return $this->get('leos.manager.user')->findOneBySlugOrFail($slug, new NotFoundHttpException("user.not_found"));
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="create a new user",
     *     statusCodes = {
     *      201 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions"
     *     },
     *     input="Leos\Bundle\UserBundle\Form\UserType",
     *     responseMap={
     *      201 = "Leos\Bundle\UserBundle\Entity\User"
     *     }
     * )
     *
     * @Rest\RequestParam(
     *     name="username",
     *     strict=false,
     *     description="Username, must be unique"
     * )
     *
     * @Rest\RequestParam(
     *     name="plain_password",
     *     map=true,
     *     strict=false,
     *     description="The user plain password"
     * )
     *
     * @Rest\RequestParam(
     *     name="state",
     *     strict=false,
     *     description="The user state"
     * )
     *
     * @Rest\RequestParam(
     *     name="currency",
     *     strict=false,
     *     description="The user currency"
     * )
     *
     * @Rest\View(statusCode=201)
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\FormInterface
     */
    public function postAction(ParamFetcher $paramFetcher)
    {
        try {
            /** @var User $user */
            $user = $this->get('leos.manager.user')->create($paramFetcher->all());

            return $this->routeRedirectView(
                'get_user',
                [
                    'slug' => $user->getSlug(),
                ],
                Response::HTTP_CREATED
            );
        } catch (UserAlreadyExistException $e) {

            throw new ConflictHttpException($e->getMessage());

        }catch (FormException $e) {

            return $e->getForm();
        }
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Update user information",
     *     statusCodes = {
     *      202 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions"
     *     }
     * )
     *
     * @Rest\RequestParam(
     *     name="username",
     *     strict=false,
     *     description="Username, must be unique"
     * )
     * @Rest\RequestParam(
     *     name="plain_password",
     *     map=true,
     *     strict=false,
     *     description="The user plain password"
     * )
     *
     * @Rest\RequestParam(
     *     name="state",
     *     strict=false,
     *     description="The user state"
     * )
     * @Rest\RequestParam(
     *     name="currency",
     *     strict=false,
     *     description="The user currency"
     * )
     *
     * @Rest\View(statusCode=202)
     *
     * @param string $slug
     * @param ParamFetcher $paramFetcher
     *
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\FormInterface
     */
    public function putAction(string $slug, ParamFetcher $paramFetcher)
    {
        try {

            $user = $this->get('leos.manager.user')->replace($paramFetcher->all(), $this->getAction($slug));

            return $this->routeRedirectView(
                'get_users',
                [
                    'slug' => $user->getSlug()
                ],
                Response::HTTP_ACCEPTED
            );

        } catch (FormException $e) {

            return $e->getForm();

        }
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Update user information",
     *     statusCodes = {
     *      202 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions"
     *     }
     * )
     *
     * @Rest\RequestParam(
     *     name="username",
     *     strict=false,
     *     description="Username, must be unique"
     * )
     * @Rest\RequestParam(
     *     name="plain_password",
     *     map=true,
     *     strict=false,
     *     description="The user plain password"
     * )
     *
     * @Rest\RequestParam(
     *     name="state",
     *     strict=false,
     *     description="The user state"
     * )
     * @Rest\RequestParam(
     *     name="currency",
     *     strict=false,
     *     description="The user currency"
     * )
     *
     * @Rest\View(statusCode=202)
     *
     * @param string $slug
     * @param Request $request
     *
     * @return \Leos\Component\User\Model\User|\Symfony\Component\Form\FormInterface
     */
    public function patchAction(string $slug, Request $request)
    {
        try {

            $user = $this->get('leos.manager.user')->update($request->request->all(), $this->getAction($slug));

            return $this->routeRedirectView(
                'get_users',
                [
                    'slug' => $user->getSlug()
                ],
                Response::HTTP_ACCEPTED
            );

        } catch (FormException $e) {

            return $e->getForm();
        }
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     description="Delete a user",
     *     statusCodes = {
     *      204 = "Returned when successful",
     *      400 = "Returned when invalid parameters",
     *      401 = "Returned when no authorized",
     *      403 = "Returned when no permissions",
     *      404 = "Returned when user not found"
     *     }
     * )
     *
     * @param string $slug
     *
     * @return \FOS\RestBundle\View\View
     */
    public function deleteAction(string $slug)
    {
        $this->get('leos.manager.user')->delete($this->getAction($slug));

        return $this->routeRedirectView('get_users', [], Response::HTTP_NO_CONTENT);
    }
}
