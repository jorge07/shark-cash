<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Hateoas\Representation\PaginatedRepresentation;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package AppBundle\Controller
 */
class Controller extends FOSRestController
{
    /**
     * @param string $route
     * @param array $parameters
     * @param int $statusCode
     * @param array $headers
     * @return \FOS\RestBundle\View\View
     */
    protected function routeRedirectView(
        $route,
        array $parameters = [],
        $statusCode = Response::HTTP_CREATED,
        array $headers = [])
    {
        return parent::routeRedirectView(
            $route,
            array_merge([
                'version' => $this->getVersion(),
                '_format' => $this->getFormat()
            ],
            $parameters),
            $statusCode,
            array_merge(
                $this->get('request_stack')->getMasterRequest()->headers->all(),
                $headers
            )
        );
    }

    /**
     * @param Pagerfanta $pagination
     * @param string $route
     * @param array $params
     * @param int|null $limit
     * @param int $page
     * @return PaginatedRepresentation
     */
    protected function paginate(
        Pagerfanta $pagination,
        string $route,
        array $params = [],
        int $limit = null,
        int $page = 1): PaginatedRepresentation
    {
        return $this->get('leos.utils.pagination')->paginate(
            $pagination, $route, $params, $limit, $page
        );
    }
    /**
     * @return mixed
     */
    protected function getFormat()
    {
        return $this->get('leos.utils.api')->getFormat();
    }

    /**
     * @return mixed
     */
    protected function getVersion()
    {
        return $this->get('leos.utils.api')->getVersion();
    }

}
