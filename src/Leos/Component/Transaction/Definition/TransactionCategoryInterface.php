<?php
namespace Leos\Component\Transaction\Definition;

use Leos\Component\Transaction\Model\TransactionCategory;

/**
 * Class TransactionCategoryInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Definition
 */
interface TransactionCategoryInterface
{

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return TransactionCategory
     */
    public function setName($name): TransactionCategory;

    /**
     * @return string
     */
    public function getType(): string;
}