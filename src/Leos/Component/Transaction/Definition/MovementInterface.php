<?php

namespace Leos\Component\Transaction\Definition;

use Leos\Component\Transaction\Model\TransactionCategory;
use Leos\Component\Wallet\Definition\CreditInterface;

/**
 * Class MovementInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Definition
 */
interface MovementInterface
{
    /**
     * MovementInterface constructor.
     * @param TransactionCategory $category
     * @param float $real
     * @param float $bonus
     */
    public function __construct(TransactionCategory $category, float $real = 0.00, float $bonus = 0.00);

    /**
     * @return CreditInterface
     */
    public function getReal(): CreditInterface;

    /**
     * @return CreditInterface
     */
    public function getBonus(): CreditInterface;

    /**
     * @param float $amount
     *
     * @return MovementInterface
     */
    public function setReal(float $amount): MovementInterface;

    /**
     * @param float $amount
     *
     * @return MovementInterface
     */
    public function setBonus(float $amount): MovementInterface;

    /**
     * @return TransactionCategory
     */
    public function getCategory(): TransactionCategory;

    /**
     * @param TransactionCategory $transactionCategory
     * @return MovementInterface
     */
    public function setCategory(TransactionCategory $transactionCategory): MovementInterface;
}
