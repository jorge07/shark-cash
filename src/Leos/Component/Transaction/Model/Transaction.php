<?php
namespace Leos\Component\Transaction\Model;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Wallet\Definition\CreditInterface;
use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Definition\TransactionInterface;

/**
 * Class Transaction
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\Model
 */
class Transaction implements TransactionInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id = 0;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var TransactionCategory
     */
    protected $category;

    /**
     * @var int
     */
    protected $amountReal = 0;

    /**
     * @var int
     */
    protected $amountBonus = 0;

    /**
     * @var int
     */
    protected $amountRealOld = 0;

    /**
     * @var int
     */
    protected $amountBonusOld = 0;

    /**
     * @var int
     */
    protected $amountRealOperation = 0;

    /**
     * @var int
     */
    protected $amountBonusOperation = 0;

    /**
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * Transaction constructor.
     * @param UserInterface $user
     * @param MovementInterface|null $movementInterface
     */
    public function __construct(UserInterface $user, MovementInterface $movementInterface)
    {
        /** @var UserInterface|WalletAwareInterface $user */
        $this->setUser($user, $movementInterface);
        $this->category = $movementInterface->getCategory();
        $this->createdAt = new \DateTime();
        $this->setAmountReal($movementInterface->getReal());
        $this->setAmountBonus($movementInterface->getBonus());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface|WalletAwareInterface $user
     * @param MovementInterface $movement
     *
     * @throws \Exception
     */
    private function setUser($user, MovementInterface $movement)
    {

        $this->amountRealOld = $user->getWallet()->getRealCredit();
        $this->amountBonusOld = $user->getWallet()->getBonusCredit();

        if ($movement instanceof DebitMovement) {

            $user = $this->debit($user, $movement);

        } else if ($movement instanceof CreditMovement) {

            $user = $this->credit($user, $movement);

        }

        $this->user = $user;

        $this->currency = $user->getCurrency();

    }

    /**
     * @return TransactionCategory
     */
    public function getCategory(): TransactionCategory
    {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getAmountReal(): int
    {
        return $this->amountReal;
    }

    /**
     * @param CreditInterface $amountReal
     * @return Transaction
     */
    public function setAmountReal(CreditInterface $amountReal): Transaction
    {
        $this->amountReal = $amountReal->getAmount();

        return $this;
    }

    /**
     * @return int
     */
    public function getAmountBonus(): int
    {
        return $this->amountBonus;
    }

    /**
     * @param CreditInterface $amountBonus
     * @return Transaction
     */
    public function setAmountBonus(CreditInterface $amountBonus)
    {
        $this->amountBonus = $amountBonus->getAmount();

        return $this;
    }

    /**
     * @return int
     */
    public function getAmountRealOld(): int
    {
        return $this->amountRealOld;
    }


    /**
     * @return int
     */
    public function getAmountBonusOld(): int
    {
        return $this->amountBonusOld;
    }

    /**
     * @return int
     */
    public function getAmountRealOperation(): int
    {
        return $this->amountRealOperation;
    }

    /**
     * @return int
     */
    public function getAmountBonusOperation(): int
    {
        return $this->amountBonusOperation;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency(): CurrencyInterface
    {
        return $this->currency;
    }


    /**
     * @param WalletAwareInterface $walletAwareInterface
     * @param DebitMovement $movement
     *
     * @return WalletAwareInterface
     */
    private function debit(WalletAwareInterface $walletAwareInterface, DebitMovement $movement): WalletAwareInterface
    {
        $walletAwareInterface
            ->getWallet()
            ->debit($movement)
        ;

        $this->amountRealOperation = - $real->getAmount();
        $this->amountBonusOperation = - $bonus->getAmount();

        return $walletAwareInterface;
    }

    /**
     * @param WalletAwareInterface $walletAwareInterface
     * @param CreditMovement $movement
     *
     * @return WalletAwareInterface
     */
    private function credit(WalletAwareInterface $walletAwareInterface, CreditMovement $movement): WalletAwareInterface
    {
        $walletAwareInterface
            ->getWallet()
            ->addRealCredit($real = $movement->getReal())
            ->addBonusCredit($bonus = $movement->getBonus())
        ;

        $this->amountRealOperation = $real->getAmount();
        $this->amountBonusOperation = $bonus->getAmount();

        return $walletAwareInterface;
    }
}
