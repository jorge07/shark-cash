<?php
namespace Leos\Component\Transaction\EventListener;

use Leos\Component\Transaction\Model\Transaction;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Event\TransactionFactoryEventInterface;

/**
 * Class TransactionEventListener
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Transaction\EventListener
 */
class TransactionFactoryEventListener
{
    /**
     * @param TransactionFactoryEventInterface $event
     * @throws \Exception
     */
    public function process(TransactionFactoryEventInterface $event)
    {
        $event
            ->getTransactionAware()
            ->setTransaction(
                $this->createTransaction(
                    $event->getUser(),
                    $movement = $event->getMovement()
                )
            );

    }

    /**
     * @param UserInterface $user
     * @param MovementInterface $movementInterface
     * 
     * @return Transaction
     */
    protected function createTransaction(UserInterface $user, MovementInterface $movementInterface)
    {
        return new Transaction($user, $movementInterface);
    }
}
