<?php
namespace Leos\Component\Play\Definition;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;

/**
 * Class PlayInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Definition
 */
interface PlayInterface extends TransactionAwareInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return int
     */
    public function getGame(): int;

    /**
     * @return string
     */
    public function getSession(): string;

    /**
     * @return PlaySession
     */
    public function getPlaySession();

    /**
     * @param PlaySession $playSession
     * @return PlayInterface
     */
    public function setPlaySession(PlaySession $playSession): PlayInterface;

    /**
     * @return string
     */
    public function getRound(): string;

    /**
     * @return PlaySessionRound
     */
    public function getPlaySessionRound();

    /**
     * @param PlaySessionRound $playSessionRound
     * @return PlayInterface
     */
    public function setPlaySessionRound(PlaySessionRound $playSessionRound): PlayInterface;

    /**
     * @return float
     */
    public function getB2bReal(): float;

    /**
     * @return float
     */
    public function getB2bBonus(): float;

    /**
     * Transaction close the session
     *
     * @return boolean
     */
    public function isClose(): bool;

    /**
     * @param bool $close
     * @return PlayInterface
     */
    public function setClose(bool $close): PlayInterface;
}