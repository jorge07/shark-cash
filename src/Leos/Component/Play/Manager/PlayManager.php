<?php
namespace Leos\Component\Play\Manager;

use Leos\Component\User\Model\User;
use Leos\Component\User\Definition\UserInterface;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\Play\Factory\PlayFactoryInterface;
use Leos\Component\Play\Repository\PlayRepositoryInterface;


/**
 * Class PlayManager
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Manager
 */
class PlayManager
{
    /**
     * @var PlayFactoryInterface
     */
    private $factory;

    /**
     * @var PlayRepositoryInterface
     */
    private $playRepository;

    /**
     * PlayManager constructor.
     * @param PlayFactoryInterface $factory
     * @param PlayRepositoryInterface $playRepository
     */
    public function __construct(PlayFactoryInterface $factory, PlayRepositoryInterface $playRepository)
    {
        $this->factory = $factory;
        $this->playRepository = $playRepository;
    }

    /**
     * @param UserInterface $user
     * @param string $category
     * @param float $real
     * @param float $bonus
     * @param array $data
     * @return \Leos\Bundle\PlayBundle\Entity\Play|\Leos\Component\Transaction\Definition\TransactionAwareInterface
     */
    public function create(UserInterface $user, string $category, float $real, float $bonus, array $data)
    {
        return $this->factory->process($user, $category, $real, $bonus, $data);
    }

    /**
     * @param array $criteria
     * @param array $sort
     * @return \Pagerfanta\Pagerfanta
     */
    public function all(array $criteria = [], array $sort = [])
    {
        return $this->playRepository->findAll($criteria, $sort);
    }

    /**
     * @param User $user
     * @return \Pagerfanta\Pagerfanta
     */
    public function findByUser(User $user)
    {
        return $this->playRepository->findByUser($user);
    }

    /**
     * @param PlaySession $session
     * @return \Pagerfanta\Pagerfanta
     */
    public function findBySession(PlaySession $session)
    {
        return $this->playRepository->findBySession($session);
    }

    /**
     * @param int $id
     * @param \Exception $e
     * @return PlayInterface
     * @throws \Exception
     */
    public function findOneByIdOrFail(int $id, \Exception $e): PlayInterface
    {
        $transaction = $this->playRepository->findOneById($id);

        if (!$transaction) {

            throw $e;
        }

        return $transaction;
    }
}
