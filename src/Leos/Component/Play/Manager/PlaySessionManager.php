<?php
namespace Leos\Component\Play\Manager;


use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;
use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Play\Factory\PlaySessionFactoryInterface;
use Leos\Component\Play\Factory\PlaySessionRoundFactoryInterface;
use Leos\Component\Play\Repository\PlaySessionRepositoryInterface;
use Leos\Component\Play\Repository\PlaySessionRoundRepositoryInterface;

/**
 * Class PlaySessionManager
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Manager
 */
class PlaySessionManager
{
    /**
     * @var PlaySessionRepositoryInterface
     */
    private $sessionRepository;

    /**
     * @var PlaySessionRoundRepositoryInterface
     */
    private $roundRepository;

    /**
     * @var PlayManager
     */
    private $playManager;

    /**
     * @var PlaySessionFactoryInterface
     */
    private $sessionFactoryInterface;

    /**
     * @var PlaySessionRoundFactoryInterface
     */
    private $roundFactoryInterface;

    /**
     * PlaySessionManager constructor.
     *
     * @param PlaySessionRepositoryInterface $sessionRepository
     * @param PlaySessionRoundRepositoryInterface $roundRepository
     * @param PlaySessionFactoryInterface $sessionFactoryInterface
     * @param PlaySessionRoundFactoryInterface $roundFactoryInterface
     * @param PlayManager $playManager
     */
    public function __construct(
        PlaySessionRepositoryInterface $sessionRepository,
        PlaySessionRoundRepositoryInterface $roundRepository,
        PlaySessionFactoryInterface $sessionFactoryInterface,
        PlaySessionRoundFactoryInterface $roundFactoryInterface,
        PlayManager $playManager)
    {

        $this->sessionRepository = $sessionRepository;
        $this->roundRepository = $roundRepository;
        $this->sessionFactoryInterface = $sessionFactoryInterface;
        $this->roundFactoryInterface = $roundFactoryInterface;
        $this->playManager = $playManager;
    }

    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $criteria = [], array $sort = [])
    {
        return $this->sessionRepository->findAll($criteria, $sort);
    }

    /**
     * @param UserInterface $user
     * @param string $id
     * @param \Exception $e
     *
     * @return PlaySession|null
     * @throws \Exception
     */
    public function findSessionOrFail(UserInterface $user, string $id, \Exception $e)
    {
        $session = $this->findSession($user, $id);

        if (!$session) {

            throw $e;
        }

        return $session;
    }

    /**
     * @param int $id
     * @param \Exception $e
     *
     * @return PlaySession|null
     * @throws \Exception
     */
    public function findOneSessionByIdOrFail(int $id, \Exception $e)
    {
        $session = $this->sessionRepository->findOneById($id);

        if (!$session) {

            throw $e;
        }

        return $session;
    }

    /**
     * @param UserInterface $user
     *
     * @return \Pagerfanta\Pagerfanta
     */
    public function findUserSessions(UserInterface $user)
    {
        return $this->sessionRepository->findByUser($user);
    }

    /**
     * @param UserInterface $user
     * @param string $id
     *
     * @return PlaySession|null
     */
    protected function findSession(UserInterface $user, string $id)
    {
        return $this->sessionRepository->findOneByUserAndSession($user, $id);
    }

    /**
     * @param PlaySession $session
     * @param string $id
     * @param \Exception $e
     *
     * @return PlaySessionRound|null
     * @throws \Exception
     */
    public function findOneRoundBySessionAndRoundIdOrFail(PlaySession $session, string $id, \Exception $e)
    {
        $round = $this->findRound($session, $id);

        if (!$round) {

            throw $e;
        }

        return $round;
    }

    /**
     * @param PlaySession $session
     * @param string $id
     *
     * @return null|PlaySessionRound
     */
    protected function findRound(PlaySession $session, string $id)
    {
        return $this->roundRepository->findOneBySessionAndRound($session, $id);
    }

    /**
     * @param UserInterface $user
     * @param string $category
     * @param float $real
     * @param float $bonus
     * @param array $data
     * @return PlaySession
     */
    public function play(UserInterface $user, string $category, float $real, float $bonus, array $data)
    {
        $play = $this->playManager->create($user, $category, $real, $bonus, $data);

        return $this->process($play);
    }

    /**
     * @param PlayInterface $play
     * @return PlaySession
     */
    public function process(PlayInterface $play)
    {
        $session = $this->findSession($play->getTransaction()->getUser(), $play->getSession()); // Look if session already exist

        if (!$session) {

            $session = $this->create($play); // Create a new one from scratch
        }

        $session->addTransaction($play);

        $this->sessionRepository->save($session);

        return $session;
    }

    /**
     * @param PlayInterface $play
     * @return PlaySession
     */
    protected function create(PlayInterface $play): PlaySession
    {
        return $this->sessionFactoryInterface
            ->create(
                [
                    'user' => $play->getTransaction()->getUser()->getId(),
                    'session' => $play->getSession()
                ]) //Create a new session
        ;
    }
}
