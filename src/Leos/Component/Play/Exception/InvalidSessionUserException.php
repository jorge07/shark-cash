<?php

namespace Leos\Component\Play\Exception;

/**
 * Class InvalidSessionUserException
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Play\Exception
 */
class InvalidSessionUserException extends \Exception
{

    public function __construct()
    {
        parent::__construct('round.exception.invalid_user', 409);

    }
}