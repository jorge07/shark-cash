<?php

namespace Leos\Component\Wallet\Model;

use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\Exception\NotEnoughFoundsException;

use Leos\Component\Utils\DateTime\TimestampableTrait;
use Leos\Component\Wallet\Definition\WalletInterface;
use Leos\Component\Wallet\Definition\CreditInterface;
use Leos\Component\Wallet\Definition\CurrencyInterface;

/**
 * Class Wallet
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Model
 */
class Wallet implements WalletInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $realCredit = 0;

    /**
     * @var int
     */
    protected $bonusCredit = 0;

    /**
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * Wallet constructor.
     *
     * @param int $realCredit
     * @param int $bonusCredit
     */
    public function __construct(int $realCredit = 0, int $bonusCredit = 0)
    {
        $this->createdAt = new \DateTime();
        $this->realCredit = $realCredit;
        $this->bonusCredit = $bonusCredit;
    }

    /**
     * @return int
     */
    public function getRealCredit(): int
    {
        return $this->realCredit;
    }

    /**
     * @param int $realCredit
     * @return WalletInterface
     */
    public function setRealCredit(int $realCredit): WalletInterface
    {
        $this->realCredit = $realCredit;

        return $this;
    }

    /**
     * @return int
     */
    public function getBonusCredit(): int
    {
        return $this->bonusCredit;
    }

    /**
     * @param int $bonusCredit
     * @return WalletInterface
     */
    public function setBonusCredit(int $bonusCredit): WalletInterface
    {
        $this->bonusCredit = $bonusCredit;

        return $this;
    }

    /**
     * @param CreditInterface $credit
     * @return $this
     */
    protected function addRealCredit(CreditInterface $credit)
    {
        $this->realCredit += $credit->getAmount();

        return $this;
    }

    /**
     * @param CreditInterface $credit
     * @return $this
     */
    protected function addBonusCredit(CreditInterface $credit)
    {
        $this->bonusCredit += $credit->getAmount();

        return $this;
    }

    /**
     * @param CreditInterface $credit
     * @return $this
     */
    protected function removeRealCredit(CreditInterface $credit)
    {
        if ($this->realCredit < $credit->getAmount()) {

            throw new NotEnoughFoundsException($credit);
        }

        $this->realCredit -= $credit->getAmount();

        return $this;
    }

    /**
     * @param CreditInterface $credit
     * @return $this
     */
    protected function removeBonusCredit(CreditInterface $credit)
    {
        if ($this->bonusCredit < $credit->getAmount()) {

            throw new NotEnoughFoundsException($credit);
        }

        $this->bonusCredit -= $credit->getAmount();

        return $this;
    }

    /**
     * @param MovementInterface $movement
     * @return WalletInterface
     */
    public function debit(MovementInterface $movement): WalletInterface
    {
        $this
            ->removeRealCredit($movement->getReal())
            ->removeBonusCredit($movement->getBonus())
        ;
        
        return $this;
    }

    /**
     * @param MovementInterface $movement
     * @return WalletInterface
     */
    public function credit(MovementInterface $movement): WalletInterface
    {
        $this
            ->addRealCredit($movement->getReal())
            ->addBonusCredit($movement->getBonus())
        ;

        return $this;
    }
}
