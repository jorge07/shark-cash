<?php

namespace Leos\Component\Wallet\Definition;

use Leos\Component\Transaction\Definition\MovementInterface;

/**
 * Class WalletInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\Wallet\Definition
 */
interface WalletInterface
{
    /**
     * WalletInterface constructor.
     *
     * @param int $realCredit
     * @param int $bonusCredit
     */
    public function __construct(int $realCredit  = 0, int $bonusCredit = 0);

    /**
     * @return int
     */
    public function getRealCredit(): int;

    /**
     * @param int $realCredit
     * @return WalletInterface
     */
    public function setRealCredit(int $realCredit): WalletInterface;

    /**
     * @return int
     */
    public function getBonusCredit(): int;

    /**
     * @param int $bonusCredit
     * @return WalletInterface
     */
    public function setBonusCredit(int $bonusCredit): WalletInterface;

    /**
     * @param MovementInterface $movement
     * @return WalletInterface
     */
    public function debit(MovementInterface $movement): WalletInterface;

    /**
     * @param MovementInterface $movement
     * @return WalletInterface
     */
    public function credit(MovementInterface $movement): WalletInterface;
}
