<?php
namespace Leos\Component\User\Manager;

use Leos\Component\User\Model\User;
use Leos\Component\User\Definition\UserInterface;

/**
 * Class UserManagerInterface
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Component\User\Manager
 */
interface UserManagerInterface
{
    /**
     * @param array $criteria
     * @param array $sort
     * @return mixed
     */
    public function all(array $criteria = [], array $sort = []);

    /**
     * @param string $slug
     * @return UserInterface|null
     */
    public function findOneBySlug(string $slug);

    /**
     * @param array $data
     *
     * @return User
     */
    public function create(array $data): User;

    /**
     * @param array $data
     * @param UserInterface $user
     * @return User
     */
    public function update(array $data,  UserInterface $user): User;

    /**
     * @param array $data
     * @param UserInterface $user
     * @return User
     */
    public function replace(array $data, UserInterface $user): User;

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function delete( UserInterface $user);
}
