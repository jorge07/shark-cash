<?php

namespace Leos\Bundle\UserBundle\Manager;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\User\Exception\UserAlreadyExistException;
use Leos\Component\User\Model\User;
use Leos\Component\User\Manager\UserManagerInterface;
use Leos\Component\Wallet\Definition\WalletAwareInterface;

use Leos\Bundle\UserBundle\Factory\UserFactory;
use Leos\Bundle\UserBundle\Repository\UserRepository;

use Pagerfanta\Pagerfanta;

/**
 * Class UserManager
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UserBundle\Manager
 */
class UserManager implements UserManagerInterface
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * UserManager constructor.
     *
     * @param UserRepository $userRepo
     * @param UserFactory $userFactory
     */
    public function __construct(UserRepository $userRepo, UserFactory $userFactory)
    {
        $this->userRepo = $userRepo;
        $this->userFactory = $userFactory;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    public function create(array $data): User
    {
        $user = $this->userRepo->findOneByUsername(strtolower($data['username']));

        if ($user) {

            throw new UserAlreadyExistException();
        }

        return $this->userRepo->save($this->userFactory->create($data));
    }

    /**
     * @param array $data
     * @param UserInterface $user
     * @return User
     */
    public function update(array $data, UserInterface $user): User
    {
        return $this->userRepo->save($this->userFactory->update($data, $user));
    }

    /**
     * @param array $data
     * @param UserInterface $user
     * @return User
     */
    public function replace(array $data, UserInterface $user): User
    {
        return $this->userRepo->save(
            $this->userFactory->replace($data, $user)
        );
    }

    /**
     * @param UserInterface $user
     */
    public function delete(UserInterface $user)
    {
        $this->userRepo->save(
            $user
                ->setDeletedAt(new \DateTime())
        );
    }
    /**
     * @param array $criteria
     * @param array $sort
     * @return array|Pagerfanta
     */
    public function all(array $criteria = [], array $sort = [])
    {
        return $this->userRepo->findAll($criteria, $sort);
    }

    /**
     * @param string $slug
     * @return User
     */
    public function findOneBySlug(string $slug)
    {
        return $this->userRepo->findOneBySlug($slug);
    }
    /**
     * @param string $slug
     * @param \Exception $e
     * @return User|WalletAwareInterface
     * @throws \Exception
     */
    public function findOneBySlugOrFail(string $slug, \Exception $e): User
    {
        $user = $this->userRepo->findOneBySlug($slug);

        if (!$user) throw $e;

        return $user;
    }
}
