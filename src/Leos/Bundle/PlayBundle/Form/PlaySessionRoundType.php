<?php

namespace Leos\Bundle\PlayBundle\Form;

use Leos\Bundle\PlayBundle\Entity\PlaySession;
use Leos\Bundle\PlayBundle\Entity\PlaySessionRound;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PlaySessionRoundType
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Form
 */
class PlaySessionRoundType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('session', EntityType::class, [
                'class' => PlaySession::class,
                'mapped' => false
            ])
            ->add('roundId')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PlaySessionRound::class,
            'csrf_protection' => false,
            'empty_data' => function(FormInterface $form) {

                return new PlaySessionRound(
                    $form->get('session')->getViewData(),
                    $form->get('roundId')->getData()
                );
            }
        ));
    }
}
