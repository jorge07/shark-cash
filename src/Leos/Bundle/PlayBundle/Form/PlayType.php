<?php

namespace Leos\Bundle\PlayBundle\Form;

use Leos\Bundle\PlayBundle\Entity\Play;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class PlayType
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Form
 */
class PlayType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('session', TextType::class)
            ->add('round', TextType::class)
            ->add('close',  CheckboxType::class, [
                'label'    => 'Close Round?',
                'required' => false,
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Play::class,
            'empty_data' => function (FormInterface $form) {
                return new Play(
                    1,
                    $form->get('session')->getData() ?: "",
                    $form->get('round')->getData() ?: ""
                );
            },
        ]);
    }
}
