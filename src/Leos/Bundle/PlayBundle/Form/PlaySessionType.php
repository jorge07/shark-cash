<?php

namespace Leos\Bundle\PlayBundle\Form;

use Leos\Bundle\PlayBundle\Entity\PlaySession;
use Leos\Bundle\UserBundle\Entity\User;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class PlaySessionType
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Form
 */
class PlaySessionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class
            ])
            ->add('session', TextType::class, [
                'required' => true
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => PlaySession::class,
            'empty_data' => function (FormInterface $form) {

                return new PlaySession(
                    $form->get('user')->getData(),
                    $form->get('session')->getData() ?: ""
                );
            },
        ]);
    }
}
