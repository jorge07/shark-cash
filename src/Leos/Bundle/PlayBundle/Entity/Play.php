<?php
namespace Leos\Bundle\PlayBundle\Entity;

use Leos\Component\Play\Model\Play as BasePlay;

use Leos\Bundle\TransactionBundle\Entity\Transaction;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Timestampable;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Play
 *
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "get_play",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "id" = "expr(object.getId())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "session",
 *     href = "expr(link(object.getPlaySession(), 'self', true))",
 *     exclusion=@Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "transaction",
 *     href = "expr(link(object.getTransaction(), 'self', true))",
 *     exclusion=@Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @ORM\Table(name="play")
 * @ORM\Entity(repositoryClass="Leos\Bundle\PlayBundle\Repository\PlayRepository")
 *
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Play extends BasePlay
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $id;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer")
     */
    protected $game = 1;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="play.session.not_blank")
     * @Assert\NotNull(message="play.session.not_null")
     */
    protected $session = '';

    /**
     * @var PlaySession
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Embed", "Search"})
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\PlayBundle\Entity\PlaySession", inversedBy="playTransactions")
     * @ORM\JoinColumn(name="play_session_id", referencedColumnName="id")
     */
    protected $playSession;

    /**
     * @var PlaySessionRound
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Embed", "Search"})
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\PlayBundle\Entity\PlaySessionRound", inversedBy="playTransactions", cascade={"all"})
     * @ORM\JoinColumn(name="play_session_round_id", referencedColumnName="id")
     */
    protected $playSessionRound;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="play.round.not_blank")
     * @Assert\NotNull(message="play.round.not_null")
     */
    protected $round = '';

    /**
     * @var Transaction
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Embed", "Search"})
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\TransactionBundle\Entity\Transaction", cascade={"persist"})
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    protected $transaction;


    /**
     * Transaction close round
     *
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups("Exclude")
     */
    protected $close = false;

    /**
     * Creation date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $createdAt;

    /**
     * Last update date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Timestampable()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $updatedAt;

    /**
     * Delete date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $deletedAt;
}

