<?php

namespace Leos\Bundle\PlayBundle\Entity;

use Leos\Bundle\UserBundle\Entity\User;
use Leos\Component\Play\Definition\PlayInterface;
use Leos\Component\Play\Model\PlaySession as BaseSession;
use Leos\Component\Play\Model\PlaySessionRound as BaseRound;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Leos\Component\User\Definition\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PlaySession
 *
 * @Hateoas\Relation(
 *     "user",
 *     href = "expr(link(object.getUser(), 'self', true))",
 *     exclusion=@Hateoas\Exclusion(
 *          groups = {"Basic"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "get_playsession",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "id" = "expr(object.getId())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "plays",
 *     href = @Hateoas\Route(
 *          "get_playsessions_play",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "id" = "expr(object.getId())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"Basic"}
 *     )
 * )
 *
 * @ORM\Table(name="play_session")
 * @ORM\Entity(repositoryClass="Leos\Bundle\PlayBundle\Repository\PlaySessionRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class PlaySession extends BaseSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="game", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $game;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $user;

    /**
     * @var int
     *
     * @ORM\Column(name="provider", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $provider = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="session", type="string", length=255)
     *
     * @Assert\NotBlank(message="play_session.session.not_blank")
     * @Assert\NotNull(message="play_session.session.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $session;

    /**
     * @var int
     *
     * @ORM\Column(name="bets", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $bets = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wins", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $wins = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="betsAmountReal", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $betsAmountReal = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="winsAmountBonus", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $winsAmountBonus = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="winsAmountReal", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $winsAmountReal = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="betsAmountBonus", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $betsAmountBonus = 0;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Leos\Bundle\PlayBundle\Entity\PlaySessionRound", mappedBy="session", cascade={"persist"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $rounds;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Leos\Bundle\PlayBundle\Entity\Play", mappedBy="playSession", cascade={"persist"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $playTransactions;

    /**
     * PlaySession constructor.
     * @param UserInterface $user
     * @param string|null $session
     */
    public function __construct(UserInterface $user, string $session = "")
    {
        parent::__construct($user, $session);

        $this->rounds = new ArrayCollection();
        $this->playTransactions = new ArrayCollection();
    }

    /**
     * @param PlayInterface $play
     *
     * @return BaseRound
     */
    protected function createRound(PlayInterface $play): BaseRound
    {
        $exist =  false;

        $round = new PlaySessionRound($this, $play->getRound());

        $this->rounds->exists(function($item) use (&$exist, $play, $round){

            /** @var PlayInterface $item */
            if ($item->getRound() == $play->getRound()) {

                $exist = true;
            }
        });

        if ($exist) {

            $this->rounds[] = $round;
        }

        return $round;
    }
}

