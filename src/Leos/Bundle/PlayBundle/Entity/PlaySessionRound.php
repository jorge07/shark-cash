<?php

namespace Leos\Bundle\PlayBundle\Entity;

use Leos\Bundle\UserBundle\Entity\User;
use Leos\Component\Play\Model\PlaySession as BasePlaySession;
use Leos\Component\Play\Model\PlaySessionRound as BaseRound;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * PlaySessionRound
 *
 * @ORM\Table(name="play_session_round")
 * @ORM\Entity(repositoryClass="Leos\Bundle\PlayBundle\Repository\PlaySessionRoundRepository")
 */
class PlaySessionRound extends BaseRound
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $user;

    /**
     * @var PlaySession
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\PlayBundle\Entity\PlaySession", inversedBy="rounds")
     * @ORM\JoinColumn(name="play_session_id", referencedColumnName="id", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $session;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="play_session_round.round.not_blank")
     * @Assert\NotNull(message="play_session_round.round.not_null")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $roundId;

    /**
     * @var Play[]
     *
     * @ORM\OneToMany(targetEntity="Leos\Bundle\PlayBundle\Entity\Play", mappedBy="playSessionRound")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Details", "Search"})
     */
    protected $playTransactions;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $closed = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $betReal = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $betBonus = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $winReal = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $winBonus = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $deletedAt;

    /**
     * PlaySessionRound constructor.
     *
     * @param BasePlaySession $session
     * @param string $round
     */
    public function __construct(BasePlaySession $session, string $round = '')
    {
        parent::__construct($session, $round);

        $this->playTransactions = new ArrayCollection();
    }

}

