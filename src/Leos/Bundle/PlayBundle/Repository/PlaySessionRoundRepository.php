<?php
namespace Leos\Bundle\PlayBundle\Repository;

use Leos\Component\Play\Model\PlaySession;
use Leos\Component\Play\Model\PlaySessionRound;
use Leos\Component\Play\Repository\PlaySessionRoundRepositoryInterface;

use Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository\EntityRepository;

/**
 * Class PlaySessionRoundRepository
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Repository
 */
class PlaySessionRoundRepository extends EntityRepository implements PlaySessionRoundRepositoryInterface
{
    /**
     * @param PlaySession $session
     * @param string $id
     * @return null|PlaySessionRound
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySessionAndRound(PlaySession $session, string $id)
    {
        return $this->createQueryBuilder('r')
            ->where('r.session = :session')
            ->andWhere('r.roundId = :round')
            ->setParameters([
                'session' => $session,
                'round' => $id
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
