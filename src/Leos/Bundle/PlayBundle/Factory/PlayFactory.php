<?php
namespace Leos\Bundle\PlayBundle\Factory;

use Leos\Component\Utils\Factory\AbstractFactory;
use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Play\Factory\PlayFactoryInterface;
use Leos\Component\Transaction\Factory\MovementFactoryInterface;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;

use Leos\Bundle\PlayBundle\Entity\Play;
use Leos\Bundle\PlayBundle\Form\PlayType;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;
use Leos\Bundle\TransactionBundle\Event\TransactionFactoryEvent;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class PlayFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Factory
 */
class PlayFactory implements PlayFactoryInterface
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var MovementFactoryInterface
     */
    private $movementFactoryInterface;

    /**
     * PlayFactory constructor.
     *
     * @param FormFactory $formFactory
     * @param EventDispatcherInterface $eventDispatcher
     * @param MovementFactoryInterface $movementFactoryInterface
     */
    public function __construct(
        FormFactory $formFactory,
        EventDispatcherInterface $eventDispatcher,
        MovementFactoryInterface $movementFactoryInterface)
    {
        $this->formFactory = $formFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->movementFactoryInterface = $movementFactoryInterface;
    }

    /**
     * @param UserInterface $user
     * @param string $category
     * @param float $real
     * @param float $bonus
     * @param array $data
     * @return TransactionAwareInterface|Play
     */
    public function process(UserInterface $user, string $category, float $real, float $bonus, array $data): TransactionAwareInterface
    {
        $movement = $this->movementFactoryInterface->create($category, $real, $bonus);

        $event = new TransactionFactoryEvent($user, $movement, $this->processForm($data));

        $this->eventDispatcher->dispatch(TransactionFactoryEvent::TRANSACTION_FACTORY, $event);

        return $event->getTransactionAware();
    }

    /**
     * @param array $data
     * @return Play
     * @throws FormException
     */
    protected function processForm(array $data): Play
    {
        $form = $this->getForm($data);

        if (!$form->isValid()) {

            throw (new FormException())->setForm($form);
        }

        return $form->getData();
    }

    /**
     * @param array $data
     * @return Form
     */
    protected function getForm(array $data): Form
    {
        return $this->formFactory
            ->create(PlayType::class, null, ['method' => $method = AbstractFactory::CREATE])
            ->submit($data, AbstractFactory::UPDATE !== $method);
    }
}
