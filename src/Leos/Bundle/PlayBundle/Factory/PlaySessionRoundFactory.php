<?php
namespace Leos\Bundle\PlayBundle\Factory;

use Symfony\Component\Form\FormFactory;

use Leos\Component\Utils\Factory\AbstractFactory;
use Leos\Component\Play\Model\PlaySessionRound as RoundModel;
use Leos\Component\Play\Factory\PlaySessionRoundFactoryInterface;

use Leos\Bundle\PlayBundle\Entity\PlaySessionRound;
use Leos\Bundle\PlayBundle\Form\PlaySessionRoundType;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;

/**
 * Class PlaySessionRoundFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Factory
 */
class PlaySessionRoundFactory implements PlaySessionRoundFactoryInterface
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * PlaySessionRoundFactory constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param array $data
     *
     * @return RoundModel
     * @throws FormException
     */
    public function create(array $data): RoundModel
    {
        return $this->build($data, AbstractFactory::CREATE);
    }

    /**
     * @param array $data
     * @param string $method
     * @param PlaySessionRound|null $round
     * 
     * @return PlaySessionRound
     * @throws FormException
     */
    private function build(array $data, string $method, PlaySessionRound $round = null): PlaySessionRound
    {
        $form = $this->getForm($data, $method, $round);

        if (!$form->isValid()) {

            throw (new FormException())->setForm($form);
        }

        return $form->getData();
    }

    /**
     * @param array $data
     * @param string $method
     * @param PlaySessionRound|null $round
     *
     * @return $this|\Symfony\Component\Form\FormInterface
     */
    private function getForm(array $data, string $method, PlaySessionRound $round = null)
    {
        return $this->formFactory
            ->create(PlaySessionRoundType::class, $round, ['method' => $method])
            ->submit($data, AbstractFactory::UPDATE !== $method);
    }
}
