<?php
namespace Leos\Bundle\PlayBundle\Factory;

use Symfony\Component\Form\FormFactory;

use Leos\Component\Utils\Factory\AbstractFactory;
use Leos\Component\Play\Model\PlaySession as SessionModel;
use Leos\Component\Play\Factory\PlaySessionFactoryInterface;

use Leos\Bundle\PlayBundle\Entity\PlaySession;
use Leos\Bundle\PlayBundle\Form\PlaySessionType;
use Leos\Bundle\UtilsBundle\Exception\Form\FormException;

/**
 * Class PlaySessionFactory
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\PlayBundle\Factory
 */
class PlaySessionFactory implements PlaySessionFactoryInterface
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * PlaySessionFactory constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param array $data
     *
     * @return SessionModel
     *
     * @throws FormException
     */
    public function create(array $data): SessionModel
    {
        return $this->build($data, null, AbstractFactory::CREATE);
    }


    /**
     * @param array $data
     * @param PlaySession $session
     *
     * @return SessionModel
     *
     * @throws FormException
     */
    public function replace(array $data, PlaySession $session): SessionModel
    {
        return $this->build($data, $session, AbstractFactory::REPLACE);
    }

    /**
     * @param array $data
     * @param PlaySession $session
     *
     * @return SessionModel
     *
     * @throws FormException
     */
    public function update(array $data, PlaySession $session): SessionModel
    {
        return $this->build($data, $session, AbstractFactory::UPDATE);
    }

    /**
     * @param array $data
     * @param PlaySession|null $session
     * @param string $method
     * @return PlaySession
     * @throws FormException
     */
    private function build(array $data, PlaySession $session = null, string $method): PlaySession
    {
        $form = $this->getForm($data, $session, $method);

        if (!$form->isValid()) {

            throw (new FormException())->setForm($form);
        }

        return $form->getData();
    }

    /**
     * @param array $data
     * @param PlaySession|null $session
     * @param string $method
     * @return $this|\Symfony\Component\Form\FormInterface
     */
    private function getForm(array $data, PlaySession $session = null, string $method)
    {
        return $this->formFactory
            ->create(PlaySessionType::class, $session, ['method' => $method])
            ->submit($data, AbstractFactory::UPDATE !== $method);
    }
}
