<?php
namespace Leos\Bundle\TransactionBundle\EventListener;

use Leos\Bundle\TransactionBundle\Entity\Transaction;
use Leos\Bundle\TransactionBundle\Repository\TransactionRepository;

use Leos\Component\User\Definition\UserInterface;
use Leos\Component\Transaction\Definition\MovementInterface;
use Leos\Component\Transaction\EventListener\TransactionFactoryEventListener as Base;


/**
 * Class TransactionFactoryEventListener
 *
 * @package Leos\Bundle\TransactionBundle\EventListener
 */
class TransactionFactoryEventListener extends Base
{
    /**
     * @var TransactionRepository
     */
    private $repository;

    /**
     * TransactionFactoryEventListener constructor.
     * @param TransactionRepository $repository
     */
    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UserInterface $user
     * @param MovementInterface $movementInterface
     * @return Transaction
     */
    protected function createTransaction(UserInterface $user, MovementInterface $movementInterface)
    {
        return (new Transaction($user, $movementInterface));
    }
}
