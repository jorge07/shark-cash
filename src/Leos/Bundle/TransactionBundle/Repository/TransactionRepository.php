<?php
namespace Leos\Bundle\TransactionBundle\Repository;

use Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository\EntityRepository;
use Leos\Component\Transaction\Definition\TransactionAwareInterface;

/**
 * Class TransactionRepository
 *
 * @package Leos\Bundle\TransactionBundle\Repository
 */
class TransactionRepository extends EntityRepository
{

    /**
     * @param array $criteria
     * @param array $sort
     * @return \Pagerfanta\Pagerfanta
     */
    public function findAll(array $criteria = [], array $sort = [])
    {
        return $this->createPaginator('t', $criteria, $sort);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(int $id)
    {
        return $this->createQueryBuilder('t')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }
    
    /**
     * @param TransactionAwareInterface $transactionAwareInterface
     */
    public function save(TransactionAwareInterface $transactionAwareInterface)
    {
        $this->_em->persist($transactionAwareInterface);
        $this->_em->flush();
    }
}
