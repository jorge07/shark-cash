<?php
namespace Leos\Bundle\TransactionBundle\Entity;

use Leos\Bundle\UserBundle\Entity\User;

use Leos\Component\Wallet\Definition\CurrencyInterface;
use Leos\Component\Transaction\Model\Transaction as BaseTransaction;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="Leos\Bundle\TransactionBundle\Repository\TransactionRepository")
 *
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route(
 *          "get_transaction",
 *          parameters = {
 *              "version" = "expr(service('leos.utils.api').getVersion())",
 *              "id" = "expr(object.getId())"
 *          },
 *          absolute = true
 *     ),
 *     exclusion = @Hateoas\Exclusion(
 *          groups = {"List"}
 *     )
 * )
 *
 * @Hateoas\Relation(
 *     "user",
 *     href = "expr(link(object.getUser(), 'self', true))",
 *     exclusion=@Hateoas\Exclusion(
 *          groups = {"Embed"}
 *     )
 * )
 */
class Transaction extends BaseTransaction
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttribute
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $id = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @Serializer\Expose
     * @Serializer\Groups("Details")
     */
    protected $user;

    /**
     * @var TransactionCategory
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\TransactionBundle\Entity\TransactionCategory")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountReal;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountRealOld;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountRealOperation;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountBonus;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountBonusOld;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $amountBonusOperation;


    /**
     * @var CurrencyInterface
     *
     * @Serializer\Expose
     * @Serializer\Groups("Basic")
     *
     * @ORM\ManyToOne(targetEntity="Leos\Bundle\I18nBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    protected $currency;

    /**
     * Creation date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $createdAt;

    /**
     * Last update date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $updatedAt;

    /**
     * Delete date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"Basic", "Search"})
     */
    protected $deletedAt;
}

