<?php
/**
 * Created by PhpStorm.
 * User: jorge.arco
 * Date: 09/04/16
 * Time: 20:30
 */

namespace Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository as BaseEntityRepository;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineORMAdapter;

/**
 * Class EntityRepository
 *
 * @package Leos\Bundle\UtilsBundle\Doctrine\ORM\Repository
 */
class EntityRepository extends BaseEntityRepository
{
    /**
     * @param string $alias
     * @param array $criteria
     * @param array $sorting
     *
     * @return Pagerfanta
     */
    public function createPaginator(string $alias, array $criteria = [], array $sorting = []): Pagerfanta
    {
        $queryBuilder = $this->createQueryBuilder($alias);
        $this->applyCriteria($alias, $queryBuilder, $criteria);
        $this->applySorting($alias, $queryBuilder, $sorting);
        return $this->getPaginator($queryBuilder);
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return Pagerfanta
     */
    protected function getPaginator(QueryBuilder $queryBuilder): Pagerfanta
    {
        // Use output walkers option in DoctrineORMAdapter should be false as it affects performance greatly (see #3775)
        return new Pagerfanta(new DoctrineORMAdapter($queryBuilder, true, false));
    }

    /**
     * @param array $objects
     *
     * @return Pagerfanta
     */
    protected function getArrayPaginator($objects): Pagerfanta
    {
        return new Pagerfanta(new ArrayAdapter($objects));
    }

    /**
     * @param string $alias
     * @param QueryBuilder $queryBuilder
     * @param array $criteria
     */
    protected function applyCriteria(string $alias, QueryBuilder $queryBuilder, array $criteria = [])
    {
        foreach ($criteria as $property => $value) {
            $name = $this->getPropertyName($alias, $property);
            if (null === $value) {
                $queryBuilder->andWhere($queryBuilder->expr()->isNull($name));
            } elseif (is_array($value)) {
                $queryBuilder->andWhere($queryBuilder->expr()->in($name, $value));
            } elseif ('' !== $value) {
                $parameter = str_replace('.', '_', $property);
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->eq($name, ':'.$parameter))
                    ->setParameter($parameter, $value)
                ;
            }
        }
    }

    /**
     * @param string $alias
     * @param QueryBuilder $queryBuilder
     * @param array $sorting
     */
    protected function applySorting(string $alias, QueryBuilder $queryBuilder, array $sorting = [])
    {
        foreach ($sorting as $property => $order) {
            if (!empty($order)) {
                $queryBuilder->addOrderBy($this->getPropertyName($alias, $property), $order);
            }
        }
    }

    /**
     * @param string $alias
     * @param string $name
     * @return string
     */
    protected function getPropertyName(string $alias, string $name): string
    {
        if (false === strpos($name, '.')) {
            return $alias.'.'.$name;
        }
        return $name;
    }
}
