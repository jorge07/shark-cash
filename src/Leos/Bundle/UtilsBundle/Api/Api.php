<?php
namespace Leos\Bundle\UtilsBundle\Api;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Api
 *
 * @author Jorge Arco <jorge.arcoma@gmail.com>
 * @package Leos\Bundle\UtilsBundle\Version
 */
class Api
{
    /**
     * @var null|\Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * Api constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return ($this->request)
            ? $this->request->get('version', "v1")
            : "v1"
        ;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return ($this->request)
            ? $this->request->getRequestFormat()
            : 'json'
        ;
    }
}